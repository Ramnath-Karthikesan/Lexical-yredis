import { useEffect, useState } from "react"
import Editor from "./Editor";

function App() {

    const [authToken, setAuthToken] = useState('');

    useEffect(() => {
      const start = async () => {
        const authTokenRequest = await fetch(`/api/auth/token`)
        const token = await authTokenRequest.text()
        console.log(token)
        setAuthToken(token);
      }

      start()
    }, [])

    return (
      authToken ? <Editor authToken={authToken} /> : <></>
    );
}

export default App
