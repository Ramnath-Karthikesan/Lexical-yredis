import { CollaborationPlugin } from "@lexical/react/LexicalCollaborationPlugin";
import { LexicalComposer } from "@lexical/react/LexicalComposer";
import { ContentEditable } from "@lexical/react/LexicalContentEditable";
import { PlainTextPlugin } from "@lexical/react/LexicalPlainTextPlugin";
import LexicalErrorBoundary from '@lexical/react/LexicalErrorBoundary';
import { WebsocketProvider } from "y-websocket";
import * as Y from "yjs";

interface EditorProps {
    authToken: string
}

export default function Editor({authToken}: EditorProps){

    const initialConfig = {
        // NOTE: This is critical for collaboration plugin to set editor state to null. It
        // would indicate that the editor should not try to set any default state
        // (not even empty one), and let collaboration plugin do it instead
        editorState: null,
        namespace: "Demo",
        nodes: [],
        onError: (error: Error) => {
          throw error;
        },
        theme: {},
    };

    return (
        <LexicalComposer initialConfig={initialConfig}>
          <PlainTextPlugin 
            contentEditable={<ContentEditable />}
            placeholder={<div>Enter some text...</div>}
            ErrorBoundary={LexicalErrorBoundary}
          />
          <CollaborationPlugin
            id="room1"
            // @ts-ignore
            providerFactory={(id, yjsDocMap) => {
    
              
              const doc = new Y.Doc();
              // @ts-ignore
              yjsDocMap.set(id, doc);
    
              const provider = new WebsocketProvider(
                "ws://localhost:3002",
                id,
                // @ts-ignore
                doc,
                { params: { yauth: authToken }}
              );
    
              return provider;
            }}
            
            shouldBootstrap={true}
    
          />
    
        </LexicalComposer>
    )

}