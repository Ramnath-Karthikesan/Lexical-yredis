# Lexical Yredis



## Getting started

Follow these steps to setup the project

Clone the repository

```
git clone https://gitlab.com/Ramnath-Karthikesan/Lexical-yredis.git
cd lexical-yredis
```

Install the frontend dependencies

```
npm i 
```

Install the backend dependecies

```
cd backend
npm i
```

copy the env variables from the .env.template

```
cp .env.template .env
```

Generate the Authentication credentials

```
npx 0ecdsa-generate-keypair --name auth >> .env
```

Start the frontend and backend in separate terminals

```
#frontend - repo root level
npm run dev

#backend
cd backend
npm start
```